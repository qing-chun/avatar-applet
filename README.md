# 头像小程序

#### 介绍
DIY头像小程序，国庆、圣诞、新年等头像框素材，带后台多管理员、用户管理、广告管理、多个小程序管理、多平台管理、相框管理、自定义小程序背景等。

#### 软件架构
软件架构说明


#### 安装教程

1.  更改根目录下的siteinfo.js的 "siteroot": "https://xxx.xxx.cn",
2.  更改根目录下的siteinfo.js的 "api_key": "9yxIt8glvv1EPLiyRiOPmzI7lo6jSd66"

#### 使用说明

1.  xxxx

#### 参与贡献

1.  Fork 本仓库

#### 预览

![输入图片说明](gh_b8ef443b06a2_430.jpg)

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
