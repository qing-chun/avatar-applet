const siteinfo = require('../siteinfo.js')
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

// class Util {

//     /***
//      * 按照显示图片的宽等比例缩放得到显示图片的高
//      * @params originalWidth  原始图片的宽
//      * @params originalHeight 原始图片的高
//      * @params imageWidth     显示图片的宽，如果不传就使用屏幕的宽
//      * 返回图片的宽高对象
//      ***/
//     static imageZoomHeightUtil(originalWidth, originalHeight, imageWidth) {
//         let imageSize = {};
//         if (imageWidth) {
//             imageSize.imageWidth = imageWidth;
//             imageSize.imageHeight = (imageWidth * originalHeight) / originalWidth;
//         } else { //如果没有传imageWidth,使用屏幕的宽
//             wx.getSystemInfo({
//                 success: function (res) {
//                     imageWidth = res.windowWidth;
//                     imageSize.imageWidth = imageWidth;
//                     imageSize.imageHeight = (imageWidth * originalHeight) / originalWidth;
//                 }
//             });
//         }
//         return imageSize;
//     }

//     /***
//      * 按照显示图片的高等比例缩放得到显示图片的宽
//      * @params originalWidth  原始图片的宽
//      * @params originalHeight 原始图片的高
//      * @params imageHeight    显示图片的高，如果不传就使用屏幕的高
//      * 返回图片的宽高对象
//      ***/
//     static imageZoomWidthUtil(originalWidth, originalHeight, imageHeight) {
//         let imageSize = {};
//         if (imageHeight) {
//             imageSize.imageWidth = (imageHeight * originalWidth) / originalHeight;
//             imageSize.imageHeight = imageHeight;
//         } else { //如果没有传imageHeight,使用屏幕的高
//             wx.getSystemInfo({
//                 success: function (res) {
//                     imageHeight = res.windowHeight;
//                     imageSize.imageWidth = (imageHeight * originalWidth) / originalHeight;
//                     imageSize.imageHeight = imageHeight;
//                 }
//             });
//         }
//         return imageSize;
//     }

// }

// export default Util;

// 获取数据函数
const getData = (parameter) => {
    wx.showLoading({
        title: '加载中...'
    })
    // 平台参数
    parameter.data.platform = 'wxapp'
    // 接口秘钥
    parameter.data.api_key = siteinfo.api_key
    // access_token
    let userInfo = wx.getStorageSync('userInfo')
    // 最大尝试登录次数
    let maxattempts = Number(wx.getStorageSync('maxattempts'))

    return new Promise((resolve, reject) => {
        wx.request({
            url: parameter.url,
            data: parameter.data,
            method: parameter.method,
            header: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${userInfo.access_token}`
            },
            success: res => {
                if (res.data.status_code === 200) {
                    if (res.data.status == 'error') {
                        wx.showModal({
                            title: '请求错误',
                            content: res.data.data.error,
                            complete: (res) => {
                                if (res.cancel) {}
                                if (res.confirm) {}
                            }
                        })
                    }
                    resolve(res.data)
                } else if (res.data.message == 'Unauthenticated.') {
                    // 最大尝试登录次数
                    if (maxattempts < 3) {
                        maxattempts += 1
                        wx.setStorageSync('maxattempts', maxattempts)
                    } else {
                        wx.showToast({
                            title: '请稍后再试~',
                            icon: 'none'
                        })
                        console.log('已达最大尝试次数' + 3)
                        wx.setStorageSync('maxattempts', 0)
                        return false
                    }
                    // return false
                    // 登陆过期
                    console.log('发起登录')
                    wx.login({
                        success: async res => {
                            wx.request({
                                url: siteinfo.siteroot + '/api/avatar_login',
                                data: {
                                    platform: 'wxapp',
                                    code: res.code,
                                    api_key: siteinfo.api_key
                                },
                                header: {
                                    'Accept': 'application/json'
                                },
                                method: "POST",
                                success: res => {
                                    if (res.data.status == 'SUCCESS') {
                                        console.log('登录成功', res)
                                        wx.setStorageSync('maxattempts', 0)
                                        wx.setStorageSync('userInfo', res.data.data)
                                        resolve(getData(parameter))
                                    } else {
                                        if (res.data.status == 'error') {
                                            wx.showModal({
                                                title: '请求错误',
                                                content: res.data.data.error,
                                                complete: (res) => {
                                                    if (res.cancel) {}
                                                    if (res.confirm) {}
                                                }
                                            })
                                            resolve(res.data)
                                        }
                                    }
                                }
                            })
                        }
                    })
                } else {
                    wx.showToast({
                        title: '请稍后再试',
                        icon: 'none'
                    })
                }
            },
            complete() {
                wx.hideLoading()
            }
        })
    })
}

module.exports = {
    formatTime,
    getData
}
